Use this Chrome extension to navigate to a certain site whenever you try to get to another one. For example, you can set it to take you to khanacademy.org when you type in facebook.com. It is also useful for frequent misspellings, so you can direct gooogle.com to google.com.
Go to the Chrome Extensions page, under the name of this extension, click options, and enter as many urls as you like.
