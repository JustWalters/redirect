document.addEventListener('DOMContentLoaded', function(){
	var toRemove = [],
	removeInputs = function(e){
		e.preventDefault();
		var parentSpan = e.currentTarget.parentElement;

		toRemove.push(parentSpan.children[0].value);
		parentSpan.remove();
	},
	addAllListeners = function(){
		// so, so, so inefficient
		var x = document.getElementsByClassName('remove');
		for(var i=0;i<x.length;i++){
			x[i].addEventListener('click', removeInputs);
		}
	};

	chrome.storage.sync.get(null, function(items){
		var form = document.getElementById('main-form'), length = 0, html;
		for (var from in items) {
			if (items.hasOwnProperty(from)) {
				var checked = items[from].active ? "checked" : "";
				html = '<span class="pairing">From: <input type="url" class="from-input" value="' + from.slice(9) + '"> To: <input type="url" class="to-input" value="' + items[from].val + '"><input type="checkbox"' + checked + '> <a href="#" class="remove">X</a><br></span>';

				form.insertAdjacentHTML('beforeend', html);
				length++;
			}
		}

		if (length === 0) {
			html = '<span class="pairing">From: <input type="url" class="from-input"> To: <input type="url" class="to-input"><input type="checkbox" checked> <a href="#" class="remove">X</a><br></span>';

			form.insertAdjacentHTML('beforeend', html);
		}
		addAllListeners();
	});

	document.getElementById('decide').addEventListener('click', function(e){
		e.preventDefault();
		var rows = document.getElementsByClassName('pairing'), rlength = rows.length, rsites = {};

		for (var i=0; i < rlength; i++){
			var row = rows.item(i), from = row.children[0], to = row.children[1], check = row.children[2];
			if (from.value && to.value) {
				rsites['redirect-' + from.value] = {val: to.value, active: check.checked};
			}
		}

		chrome.storage.sync.set(rsites, function(){
			var status = document.getElementById('status');
			status.textContent = 'Options saved.';
			setTimeout(function() {
				status.textContent = '';
			}, 2 * 1000);
		});
		chrome.storage.sync.remove(toRemove.map(function(el){
			return 'redirect-'+el;
		}));
	});

	document.getElementById('add-input').addEventListener('click', function(e){
		e.preventDefault();
		var html = '<span class="pairing">From: <input type="url" class="from-input"> To: <input type="url" class="to-input"><input type="checkbox" checked> <a href="#" class="remove">X</a><br></span>';

		var form = document.getElementById('main-form');
		form.insertAdjacentHTML('beforeend', html);
		addAllListeners();
	});

	addAllListeners();
});
