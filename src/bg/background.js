//example of using a message handler from the inject scripts
var getLocation = function(href) {
	return new URL(href);
},
getHost = function(url) {
	return getLocation(url).host;
},
contains = function(obj, str){
	for (var key in obj) {
		if (obj.hasOwnProperty(key)) {
			//alert(str + ' and ' + obj[key]);
			if (str === key) return true;
			if (str === 'www.' + key) return true;
		}
	}
	return false;
},
format = function(url) {
	var res = url;
	return res;
},
updateSites = function(cb) {
	var sites = {};
	chrome.storage.sync.get(null, function(items){
		for (var key in items) {
			if (items.hasOwnProperty(key)) {
				if (key.indexOf('redirect-') === 0) sites[format(key.slice(9))] = format(items[key]);
			}
		}
		cb(sites);
	});
};

chrome.webNavigation.onCommitted.addListener(function(details){
	var url = details.url;
	updateSites(function(sites){
		var isAFrom = contains(sites, getHost(url));
		if (!isAFrom) return;

		var to = sites[getHost(url)], addr = to.val, checked = to.active;
		if (checked && details.frameId === 0){
			chrome.tabs.update(details.tabId, {
				url: 'http://' + addr
			});
		}
	});
});
